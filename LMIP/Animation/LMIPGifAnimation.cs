﻿using LMIP.Interfaces;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace LMIP.Animation
{
    public class LMIPGifAnimation : ILMIPAnimation
    {
        void ILMIPAnimation.CreateAnimation(string fileName, ELinearFilters filter)
        {
            LMIPGifAnimation.CreateAnimation(fileName, filter);
        }

        public static void CreateAnimation(string fileName, ELinearFilters filter)
        {

            Bitmap original = (Bitmap)Bitmap.FromFile(fileName);

            Bitmap image = (Bitmap)LMIPProcessor.NonLinearFilter(ENonLinearFilters.Sobel, fileName, 1).GetImage();
            GifBitmapEncoder gEnc = new GifBitmapEncoder();


            Bitmap[] images = GetIntermediateImages(original, image,60);

            foreach (Bitmap bmpImage in images)
            {
                var bmp = bmpImage.GetHbitmap();
                var src = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                    bmp,
                    IntPtr.Zero,
                    Int32Rect.Empty,
                    BitmapSizeOptions.FromEmptyOptions());
                gEnc.Frames.Add(BitmapFrame.Create(src));               
            }


            using (FileStream fs = new FileStream(fileName+"testi.gif", FileMode.Create))
            {
                gEnc.Save(fs);
            }

        }

    //Weitere Parameter: Dauer oder Frames usw.
    private static Bitmap[] GetIntermediateImages(Bitmap from, Bitmap to, int frames)
    {
            LMIPImage lFrom = new LMIPImage(from);
            LMIPImage lTo = new LMIPImage(to);
            int width = from.Width;
            int height = from.Height;
            Bitmap[] gifBitmaps = new Bitmap[frames];
            LMIPFadeTransition fadeTransition = new LMIPFadeTransition();
            fadeTransition.Prepare(lFrom);
            //for (int index =0; index<frames;++index)
            Parallel.For(0, frames, index =>
            {

                Bitmap frame = new Bitmap(width, height, PixelFormat.Format24bppRgb);
                LMIPImage lFrame = new LMIPImage(frame);

                for (int x = 0; x < width; ++x)
                    for (int y = 0; y < height; ++y)
                    {

                        int weightOld;
                        int weightNew;
                        fadeTransition.GetPxielFramePercentage(index,frames, x, y,out weightOld,out weightNew);

                        Color oldColor = lFrom.GetPixel(x, y);
                        Color newColor = lTo.GetPixel(x, y);

                        int red = (int)(oldColor.R * (float)weightOld / 100 + (float)weightNew / 100*newColor.R);
                        int green = (int)(oldColor.G * (float)weightOld / 100 + (float)weightNew / 100*newColor.G);
                        int blue = (int)(oldColor.B * (float)weightOld / 100 + (float)weightNew / 100*newColor.B);
                        lFrame.SetPixel(x, y, Color.FromArgb(red, green, blue));


                    }
                gifBitmaps[index] = (Bitmap)lFrame.GetImage();
            });

            return gifBitmaps;
        }
    }
}
