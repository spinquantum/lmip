﻿using LMIP.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMIP.Animation
{
    public class LMIPFadeTransition : ILMIPTransition
    {
        LMIPImage m_Image;
        public void GetPxielFramePercentage(int frame,int frameCount, int x, int y,out int oldPercentage, out int newPercentages)
        {
            int perc = (int)((float)frame / frameCount*100);
            oldPercentage = 100 - perc;
            newPercentages = perc;
        }

        public void Prepare(LMIPImage image)
        {
            m_Image = image;
        }
    }
}
