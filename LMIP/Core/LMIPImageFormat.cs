﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMIP
{
    public class LMIPImageFormat 
    {
        static LMIPImageFormat()
        {
            m_imageFormats = new Dictionary<EImageFormat, ImageFormat>
            {
                { EImageFormat.Jpeg, ImageFormat.Jpeg },
                { EImageFormat.Png, ImageFormat.Png },
                { EImageFormat.Bmp, ImageFormat.Bmp },
                { EImageFormat.Tiff, ImageFormat.Tiff }
            };
        }

        private static Dictionary<EImageFormat, ImageFormat> m_imageFormats;

        public static ImageFormat GetImageFormat(EImageFormat lmipImageFormat)
        {
            ImageFormat imageformat;
            if (m_imageFormats.TryGetValue(lmipImageFormat, out imageformat))
                return imageformat;
            else
                return ImageFormat.Jpeg;
        }
    }
}
