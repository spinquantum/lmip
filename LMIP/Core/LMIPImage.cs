using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;


namespace LMIP
{
    /// <summary>
    /// Encapsulates an Image and provided basic functionality
    /// </summary>
    public class LMIPImage 
    {        
        public int m_Width;
        public int m_Height;
        public int m_Stride;
        byte[] m_ImageBytes = null;

        public int[] m_CumulativeHistogramRed = new int[256];
        public int[] m_CumulativeHistogramGreen = new int[256];
        public int[] m_CumulativeHistogramBlue = new int[256];

        public LMIPImage(Image image)
        {
            m_Width = image.Width;
            m_Height = image.Height;
            int imageWidthMal3 = m_Width * 3;
            m_Stride = imageWidthMal3 + (4 - imageWidthMal3 % 4) % 4;

            int length = m_Stride * image.Height;
            m_ImageBytes = new byte[length];

            BitmapData bmpData = ((Bitmap)image).LockBits(
                                 new Rectangle(0, 0, image.Width, image.Height),
                                 ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);

            Marshal.Copy(bmpData.Scan0, m_ImageBytes, 0, length);
            ((Bitmap)image).UnlockBits(bmpData);
        }


        public void CalculateCumulativeHistogram()
        {
            int[] histogramRed = new int[256];
            int[] histogramGreen = new int[256];
            int[] histogramBlue = new int[256];


            int imageWidthMal3 = m_Width * 3;
            int stride = imageWidthMal3 + (4 - imageWidthMal3 % 4) % 4;

            for (int x = 0; x < m_Width; x++)
            {
                for (int y = 0; y < m_Height; y++)
                {

                    int index = stride * y + 3 * x;
                    int red = m_ImageBytes[index + 2];
                    int green = m_ImageBytes[index + 1];
                    int blue = m_ImageBytes[index];
                    histogramRed[red]++;
                    histogramGreen[green]++;
                    histogramBlue[blue]++;
                }
            }

            m_CumulativeHistogramRed[0] = histogramRed[0];
            m_CumulativeHistogramGreen[0] = histogramGreen[0];
            m_CumulativeHistogramBlue[0] = histogramBlue[0];

            for (int i = 1; i <= 255; i++)
            {
                m_CumulativeHistogramRed[i] = m_CumulativeHistogramRed[i - 1] + histogramRed[i];
                m_CumulativeHistogramGreen[i] += m_CumulativeHistogramGreen[i - 1] + histogramGreen[i];
                m_CumulativeHistogramBlue[i] += m_CumulativeHistogramBlue[i - 1] + histogramBlue[i];
            }
            int redMax = m_CumulativeHistogramRed[255] / 255;
            int greenMax = m_CumulativeHistogramGreen[255] / 255;
            int blueMax = m_CumulativeHistogramBlue[255] / 255;

            for (int i = 0; i <= 255; i++)
            {
                m_CumulativeHistogramRed[i] /= redMax>0?redMax:1;
                m_CumulativeHistogramGreen[i] /= greenMax>0?greenMax:1;
                m_CumulativeHistogramBlue[i] /= blueMax>0?blueMax:1;
            }
        }



        public Color GetPixel(int x, int y)
        {
            int index = m_Stride * y + 3 * x;
            int red = this[index + 2];
            int green = this[index + 1];
            int blue = this[index];
            return Color.FromArgb(red, green, blue);
        }
        public void SetPixel(int x, int y, Color color)
        {
            int index = m_Stride * y + 3 * x;
            m_ImageBytes[index + 2] = color.R;
            m_ImageBytes[index + 1] = color.G;
            m_ImageBytes[index] = color.B;
        }        

        public Image GetImage()
        {            
            Bitmap bmp = new Bitmap(m_Width, m_Height, PixelFormat.Format24bppRgb);           
            BitmapData bmpData = bmp.LockBits(
                                 new Rectangle(0, 0, bmp.Width, bmp.Height),
                                 ImageLockMode.WriteOnly, bmp.PixelFormat);            
            Marshal.Copy(m_ImageBytes, 0, bmpData.Scan0, m_ImageBytes.Length);            
            bmp.UnlockBits(bmpData);
            return bmp;
        }

        public Color this[int x, int y]
        {
            get { return GetPixel(x, y); }
            set { SetPixel(x, y, value); }
        }

        public byte this[int i]
        {
            get { return m_CopyBytes==null? m_ImageBytes[i]:m_CopyBytes[i]; }
            set { m_ImageBytes[i] = value; }
        }

        public Image m_Image;

        /// <summary>
        /// Saves this <see cref="LMIPImage"/> to the specified file in existing format 
        /// </summary>
        /// <param name="filename">requires full path as string including filename of the file to be saved</param>
        public void Save(string filename)
        {
            GetImage().Save(filename);
        }

        /// <summary>
        /// Saves this <see cref="LMIPImage"/> to the specified file in specified format 
        /// </summary>
        /// <param name="filename">requires full path as string including filename of the file to be saved</param>
        /// <param name="lmipImageFormat">requires format of the image</param>
        public void Save(string filename, EImageFormat lmipImageFormat)
        {
            GetImage().Save(filename, LMIPImageFormat.GetImageFormat(lmipImageFormat));
        }

        byte[] m_CopyBytes = null;
        public void SetNonlinearMode()
        {
            m_CopyBytes = (byte[])m_ImageBytes.Clone();

        }
    }
}