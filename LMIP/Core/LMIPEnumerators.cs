﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMIP
{
    public enum ELinearFilters
    {
        OptimizeContrast,
        InvertColors
    }

    public enum ENonLinearFilters
    {
        Median,
        Sobel
    }

    public enum EImageFormat
    {
        Jpeg,
        Png,
        Bmp,
        Tiff,
        Gif
    }
}
