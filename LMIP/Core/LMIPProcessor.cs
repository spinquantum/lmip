﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using LMIP.Interfaces;
using LMIP.LinearFilters;

namespace LMIP
{
    [Serializable]
    public class InvalidKernelSizeException : Exception
    {
        public InvalidKernelSizeException()
        {
        }

        public InvalidKernelSizeException(string message)
            : base(message)
        {
        }

        public InvalidKernelSizeException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }

    public class LMIPProcessor
    {

        private static ILMIPLinearFilter GetFilterByEnum(ELinearFilters eLinearFilter)
        {
            ILMIPLinearFilter filter = null;
            switch (eLinearFilter)
            {
                case ELinearFilters.OptimizeContrast: filter = new ContrastOptimizer(); break;
                case ELinearFilters.InvertColors: filter = new ColorInverter(); break;
            }
            if (filter == null)
            {
                //Zweite exception für ungültigen Enum
            }
            return filter;
        }

        private static ILMIPNonLinearFilter GetFilterByEnum(ENonLinearFilters eNonLinearFilter)
        {
            ILMIPNonLinearFilter filter = null;
            switch (eNonLinearFilter)
            {
                case ENonLinearFilters.Median: filter = new LMIPMedianFilter(); break;
                case ENonLinearFilters.Sobel: filter = new LMIPSobelFilter(); break;
            }
            if (filter == null)
            {
                //Zweite exception für ungültigen Enum
            }
            return filter;
        }

        public static LMIPImage LinearFilter(ELinearFilters eLinearFilter, string fileName,int kernelSize=1)
        {
            ILMIPLinearFilter filter = GetFilterByEnum(eLinearFilter);
            return LinearFilter(filter, fileName,kernelSize);
        }

        public static LMIPImage[] LinearFilterBatch(ELinearFilters eLinearFilter, string[] fileNames, int kernelSize = 1)
        {
            ILMIPLinearFilter filter = GetFilterByEnum(eLinearFilter);
            return LinearFilterBatch(filter, fileNames, kernelSize);
        }

        //Hirs-Idee: Anzahl verwendeter Cores (min.1, max max :)) festlegen
        public static LMIPImage LinearFilter(ILMIPLinearFilter linearFilter, string fileName,int kernelSize = 1)
        {
            if (kernelSize <= 0)
            {
                throw new InvalidKernelSizeException("kernel size must be larger than 0");
            }
            linearFilter.KernelSize = kernelSize;
            LMIPImage image = new LMIPImage(Bitmap.FromFile(fileName));
            if (linearFilter == null) return image;             
            if(linearFilter.KernelSize>image.m_Width || linearFilter.KernelSize > image.m_Height)
            {
                throw new InvalidKernelSizeException("kernel size must not be larger than image width or height");
            }
            linearFilter.Image = image;

            linearFilter.PreProcess();

            Parallel.For(0, image.m_Width / kernelSize, theX =>
              {
                  int  x = theX * kernelSize;
                //for (int x = 0; x < image.m_Image.Width; x += kernelSize)
                for (int y = 0; y < image.m_Height; y += kernelSize)
                  {
                      int xEnd = x + kernelSize;
                      if (xEnd > image.m_Width) xEnd = image.m_Width;
                      int yEnd = y + kernelSize;
                      if (yEnd > image.m_Height) yEnd = image.m_Height;
                      linearFilter.ApplyKernel(x, y,xEnd,yEnd);
                  }
              });

            linearFilter.PostProcess();

            return image;
        }

        public static LMIPImage[] LinearFilterBatch(ILMIPLinearFilter filter, string[] fileNames, int kernelSize = 1)
        {
            LMIPImage[] retVal = new LMIPImage[fileNames.Length];
            for (int i = 0; i < fileNames.Length; ++i)
            {
                retVal[i] = LinearFilter(filter, fileNames[i], kernelSize);
            }
            return retVal;
        }

       
        public static LMIPImage NonLinearFilter(ENonLinearFilters eNonLinearFilter, string fileName, int kernelSize = 1)
        {         
            ILMIPNonLinearFilter filter = GetFilterByEnum(eNonLinearFilter);            
            return NonLinearFilter(filter, fileName, kernelSize);
        }

        public static LMIPImage[] NonLinearFilterBatch(ENonLinearFilters eNonLinearFilter, string[] fileNames, int kernelSize = 1)
        {
            ILMIPNonLinearFilter filter = GetFilterByEnum(eNonLinearFilter);
            return NonLinearFilterBatch(filter, fileNames, kernelSize);
        }

        public static LMIPImage NonLinearFilter(ILMIPNonLinearFilter nonLinearFilter, string fileName, int kernelSize = 1)
        {
            if (kernelSize <= 0)
            {
                throw new InvalidKernelSizeException("kernel size must be larger than 0");
            }
            nonLinearFilter.KernelSize = kernelSize;
            LMIPImage image = new LMIPImage(Bitmap.FromFile(fileName));
            if (nonLinearFilter == null) return image;
            if (nonLinearFilter.KernelSize > image.m_Width || nonLinearFilter.KernelSize > image.m_Height)
            {
                throw new InvalidKernelSizeException("kernel size must not be larger than image width or height");
            }
            nonLinearFilter.Image = image;
            image.SetNonlinearMode();
            nonLinearFilter.PreProcess();

            Parallel.For(0, image.m_Width, x =>
            {
                for (int y = 0; y < image.m_Height; ++y)
                {
                    if (x - kernelSize < 0) continue;
                    if (x + kernelSize >= image.m_Width) continue;
                    if (y - kernelSize < 0) continue;
                    if (y + kernelSize >= image.m_Height) continue;
                    nonLinearFilter.ApplyKernel(x, y, kernelSize, kernelSize);
                }

            }
            );
            nonLinearFilter.PostProcess();


            return image;
        }

        public static LMIPImage[] NonLinearFilterBatch(ILMIPNonLinearFilter filter, string[] fileNames, int kernelSize = 1)
        {
            LMIPImage[] retVal = new LMIPImage[fileNames.Length];
            for (int i = 0; i < fileNames.Length; ++i)
            {
                retVal[i] = NonLinearFilter(filter, fileNames[i], kernelSize);
            }
            return retVal;
        }
    }
}
