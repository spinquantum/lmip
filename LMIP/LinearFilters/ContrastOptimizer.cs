﻿using LMIP.Interfaces;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMIP.LinearFilters
{

    public class ContrastOptimizer : ILMIPLinearFilter
    {
        public int KernelSize { get; set; }
        public LMIPImage Image { get; set; }

        public void ApplyKernel(int x, int y, int width, int height)
        {
            for (int myX = x; myX < width; ++myX)
                for (int myY = y; myY < height; ++myY)
                {
                    Color oldColor = Image.GetPixel(myX, myY);
                    int red = Image.m_CumulativeHistogramRed[oldColor.R];
                    int green = Image.m_CumulativeHistogramGreen[oldColor.G];
                    int blue = Image.m_CumulativeHistogramBlue[oldColor.B];
                    Color newColor = Color.FromArgb(red, green, blue);
                    Image[myX, myY] = newColor;
                }
        }

        public void PreProcess()
        {
            Image.CalculateCumulativeHistogram();
        }

        public void PostProcess()
        {

        }
    }
}
