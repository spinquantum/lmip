﻿using LMIP.Interfaces;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMIP.LinearFilters
{
    public class ColorInverter : ILMIPLinearFilter
    {
        public int KernelSize { get; set; }

        public LMIPImage Image { get; set; }

        public void ApplyKernel(int x, int y, int width, int height)
        {
            for (int myX = x; myX < width; ++myX)
                for (int myY = y; myY < height; ++myY)
                {
                    Color oldColor = Image.GetPixel(myX, myY);
                    Image.SetPixel(myX, myY, Color.FromArgb(255 - oldColor.R, 255 - oldColor.G, 255 - oldColor.B));
                }
        }
        public void PostProcess()
        {
        }
        public void PreProcess()
        {
        }
        
        
    }
}
