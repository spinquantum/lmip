﻿using LMIP.Interfaces;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMIP
{
    /// <summary>
    /// Median-Filter for removing noise in an image (non-linear filter)
    /// </summary>
    public class LMIPMedianFilter : ILMIPNonLinearFilter
    {
        public int KernelSize { get; set; }
        public LMIPImage Image { get; set; }

        public void ApplyKernel(int x, int y, int width, int height)
        {
            int stride = (width + height + 1);
            int area = stride * stride;
            
            int[] redValues = new int[area];
            int[] greenValues = new int[area];
            int[] blueValues = new int[area];

            int index = 0;
            
            for (int xb = x-width; xb <= x + width; ++xb)
                for (int yb = y-height; yb <= y+height; ++yb)
                {
                    Color pixelColor = Image[xb, yb];
                    redValues[index] = pixelColor.R;
                    greenValues[index] = pixelColor.G;
                    blueValues[index] = pixelColor.B;
                    index++;
                }

            int newRed = Median(redValues);
            int newGreen = Median(greenValues);
            int newBlue = Median(blueValues);
            Color newColor = Color.FromArgb(newRed, newGreen, newBlue);
            Image[x, y] = newColor;
        }

       
        public void PostProcess()
        {
         
        }

        public void PreProcess()
        {
         
        }

        public int Median(int[] numbers)
        {
            if (numbers.Count() == 0)
                return 0;
            int halfIndex = numbers.Count() / 2;
            var sortedNumbers = numbers.OrderBy(n => n);
            if ((numbers.Count() % 2) == 0)
            {
                return ((sortedNumbers.ElementAt(halfIndex) + sortedNumbers.ElementAt(halfIndex - 1)) / 2);
            }
            else
            {
                return sortedNumbers.ElementAt(halfIndex);
            }
        }
    }
}
