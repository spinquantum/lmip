﻿using LMIP.Interfaces;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMIP
{
    /// <summary>
    /// Sobel Filter for Edge Detection (non-linear filter)
    /// </summary>
    public class LMIPSobelFilter : ILMIPNonLinearFilter
    {
        public int KernelSize { get; set; }
        public LMIPImage Image { get; set; }

        public void PreProcess()
        {
         
        }

        public void ApplyKernel(int x, int y, int width, int height)
        {
            width = 1;
            height = 1;

            int xSobel = 0;
            int ySobel = 0;

            int newRedx = 0;
            int newGreenx = 0;
            int newBluex = 0;
            int newRedy = 0;
            int newGreeny = 0;
            int newBluey = 0;


            for (int xb = x - width; xb <= x + width; ++xb)
            {
                for (int yb = y - height; yb <= y + height; ++yb)
                {
                    int sobelValueX = SobelX[xSobel, ySobel];
                    int sobelValueY = SobelY[xSobel, ySobel];

                    Color pixelColor = Image[xb, yb];
                    newRedx   += pixelColor.R * sobelValueX;
                    newGreenx += pixelColor.G * sobelValueX;
                    newBluex  += pixelColor.B * sobelValueX;

                    newRedy   += pixelColor.R * sobelValueY;
                    newGreeny += pixelColor.G * sobelValueY;
                    newBluey  += pixelColor.B * sobelValueY;                  

                    ySobel++;
                }
                ySobel = 0;
                xSobel++;
            }


            byte valRed = (byte)Math.Floor(Math.Sqrt((newRedx * newRedx) + (newRedy * newRedy)));
            byte valGreen = (byte)Math.Floor(Math.Sqrt((newGreenx * newGreenx) + (newGreeny * newGreeny)));
            byte valBlue = (byte)Math.Floor(Math.Sqrt((newBluex * newBluex) + (newBluey * newBluey)));

            if (valRed > 255) valRed = 255;
            if (valGreen > 255) valGreen = 255;
            if (valBlue > 255) valBlue = 255;

            Color newColor = Color.FromArgb(valRed, valGreen, valBlue);
            Image[x, y] = newColor;
        }

        public void PostProcess()
        {
         
        }

        /// <summary>
        /// Kernel for horizontal filter
        /// </summary>
        private static int[,] SobelX
        {
            get
            {
                return new int[,]
                {
                    { -1, 0, 1 },
                    { -2, 0, 2 },
                    { -1, 0, 1 }
                };
            }
        }

        /// <summary>
        /// Kernel for vertical filter
        /// </summary>
        private static int[,] SobelY
        {
            get
            {
                return new int[,]
                {
                    {  1,  2,  1 },
                    {  0,  0,  0 },
                    { -1, -2, -1 }
                };
            }
        }

    }
}
