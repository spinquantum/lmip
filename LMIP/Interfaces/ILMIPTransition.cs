﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMIP.Interfaces
{
    public interface ILMIPTransition
    {
        void Prepare(LMIPImage image);
        void GetPxielFramePercentage(int frame, int frameCount, int x, int y, out int oldPercentage, out int newPercentages);
    }
}
