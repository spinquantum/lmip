﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMIP.Interfaces
{
    public interface ILMIPLinearFilter
    {
        int KernelSize { get; set; }
        LMIPImage Image { get; set; }
        void PreProcess();
        void ApplyKernel(int x, int y, int width, int height);
        void PostProcess();
    }
}