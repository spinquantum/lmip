﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMIP.Interfaces
{
    interface ILMIPAnimation
    {
        void CreateAnimation(string fileName, ELinearFilters filter);
    }
}
