﻿using LMIP;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMIPTest
{
    [TestFixture]
    class LMIPMedianFilterTests
    {
        [Test]
        public void EvenNumbersCountReturnCorrectMedian()
        {
            int[] numbers = { 1, 20, 10, 10, 5, 11 };
            LMIPMedianFilter lmipFilter = new LMIPMedianFilter();

            int result = lmipFilter.Median(numbers);

            Assert.AreEqual(10, result);
        }

        [Test]
        public void UnevenNumbersCountReturnCorrectMedian()
        {
            int[] numbers = { 1, 20, 10, 5, 11 };
            LMIPMedianFilter lmipFilter = new LMIPMedianFilter();

            int result = lmipFilter.Median(numbers);

            Assert.AreEqual(10, result);
        }

        [Test]
        public void NoNumberReturnsCorrectMedian()
        {
            int[] numbers = { };
            LMIPMedianFilter lmipFilter = new LMIPMedianFilter();

            int result = lmipFilter.Median(numbers);

            Assert.AreEqual(0, result);
        }

        [Test]
        public void OneNumberReturnCorrectMedian()
        {
            int[] numbers = { 10 };
            LMIPMedianFilter lmipFilter = new LMIPMedianFilter();

            int result = lmipFilter.Median(numbers);

            Assert.AreEqual(10, result);
        }

        [Test]
        public void TwoNumbersReturnCorrectMedian()
        {
            int[] numbers = { 10, 5 };
            LMIPMedianFilter lmipFilter = new LMIPMedianFilter();

            int result = lmipFilter.Median(numbers);

            Assert.AreEqual(7, result);
        }
    }
}
