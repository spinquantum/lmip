﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMIP;
using NUnit.Framework;

namespace LMIPTest
{
    [TestFixture]
    class LMIPProcessorTests
    {
        private static readonly string JPG10x9 = "TestData\\Image10x9.jpg";

        [Test]
        public void LinearFilterThrowsExceptionWithTooBigKernel()
        {
            var filter = ELinearFilters.InvertColors;
            int kernelSize = 11;
            string path = Path.Combine(TestContext.CurrentContext.TestDirectory, JPG10x9);
            
            Assert.Throws<InvalidKernelSizeException>(() => LMIP.LMIPProcessor.LinearFilter(filter, path, kernelSize));
        }

        [Test]
        public void NonLinearFilterThrowsExceptionWithTooBigKernel()
        {
            var filter = ENonLinearFilters.Median;
            int kernelSize = 11;
            string path = Path.Combine(TestContext.CurrentContext.TestDirectory, JPG10x9);

            Assert.Throws<InvalidKernelSizeException>(() => LMIP.LMIPProcessor.NonLinearFilter(filter, path, kernelSize));
        }

        [Test]
        public void LinearFilterThrowsExceptionWithKernelSize0()
        {
            var filter = ELinearFilters.InvertColors;
            int kernelSize = 0;
            string path = Path.Combine(TestContext.CurrentContext.TestDirectory, JPG10x9);

            Assert.Throws<InvalidKernelSizeException>(() => LMIP.LMIPProcessor.LinearFilter(filter, path, kernelSize));
        }

        [Test]
        public void NonLinearFilterThrowsExceptionWithKernelSize0()
        {
            var filter = ENonLinearFilters.Median;
            int kernelSize = 0;
            string path = Path.Combine(TestContext.CurrentContext.TestDirectory, JPG10x9);

            Assert.Throws<InvalidKernelSizeException>(() => LMIP.LMIPProcessor.NonLinearFilter(filter, path, kernelSize));
        }

        /*[Test] cannot test paassing no filter, becasue it is not possible to pass null at the moment
        public void NoFilterReturnsEmptyImage()
        {
            int kernelSize = 3;
            string path = Path.Combine(TestContext.CurrentContext.TestDirectory, JPG10x9);

            var image = LMIPProcessor.linearFilter(null, path, kernelSize);
            Assert.IsNull(image.m_Image);
        }*/

    }
}
