﻿using LMIP;
using LMIP.Animation;
using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMIPTest.IntegrationTests
{
    [TestFixture]
    class LMIPIntegrationTests
    {
        private static readonly string JPGJENKINS = "TestData\\jenkins512.jpg";
        private static readonly string JPG10x9 = "TestData\\Image10x9.jpg";

        private string outputPath = "TestData\\SavedTestFiles\\";

        private LMIPImage image;

        [OneTimeSetUp]
        public void Init()
        {
            Directory.CreateDirectory(Path.Combine(TestContext.CurrentContext.TestDirectory, outputPath));

            int kernelSize = 3;
            var filter = ENonLinearFilters.Sobel;

            string imagePath = Path.Combine(TestContext.CurrentContext.TestDirectory, JPG10x9);

            image = LMIPProcessor.NonLinearFilter(filter, imagePath, kernelSize);

        }

        [OneTimeTearDown]
        public void Teardown()
        {
            //Directory.Delete(Path.Combine(TestContext.CurrentContext.TestDirectory, outputPath), true);
        }

        [Test]
        public void ColorConverterReturnsImage()
        {
            int kernelSize = 3;
            var filter = ELinearFilters.InvertColors;
            string path = Path.Combine(TestContext.CurrentContext.TestDirectory, JPG10x9);

            var imageResult = LMIPProcessor.LinearFilter(filter, path, kernelSize);


            Assert.IsNotNull(imageResult.GetImage());
        }

        [Test]
        public void ContrastOptimizerReturnsImage()
        {
            int kernelSize = 3;
            var filter = ELinearFilters.OptimizeContrast;
            string path = Path.Combine(TestContext.CurrentContext.TestDirectory, JPG10x9);

            var imageResult = LMIPProcessor.LinearFilter(filter, path, kernelSize);

            Assert.IsNotNull(imageResult.GetImage());
        }

        [Test]
        public void MedianFilterReturnsImage()
        {
            int kernelSize = 3;
            var filter = ENonLinearFilters.Median;
            string path = Path.Combine(TestContext.CurrentContext.TestDirectory, JPG10x9);

            var imageResult = LMIPProcessor.NonLinearFilter(filter, path, kernelSize);

            Assert.IsNotNull(imageResult.GetImage());
        }

        [Test]
        public void SobelFilterReturnsImage()
        {
            int kernelSize = 3;
            var filter = ENonLinearFilters.Sobel;
            string path = Path.Combine(TestContext.CurrentContext.TestDirectory, JPG10x9);

            var imageResult = LMIPProcessor.NonLinearFilter(filter, path, kernelSize);

            Assert.IsNotNull(imageResult.GetImage());
        }

        [Test]
        public void LinearFilterBatchReturnsImages()
        {
            int kernelSize = 3;
            var filter = ELinearFilters.InvertColors;
            string[] path = {
                Path.Combine(TestContext.CurrentContext.TestDirectory, JPG10x9),
                Path.Combine(TestContext.CurrentContext.TestDirectory, JPGJENKINS)
            };

            var imageResults = LMIPProcessor.LinearFilterBatch(filter, path, kernelSize);

            Assert.AreEqual(2, imageResults.Count());
            Assert.IsNotNull(imageResults[0].GetImage());
            Assert.IsNotNull(imageResults[1].GetImage());
        }

        [Test]
        public void NonLinearFilterBatchReturnsImages()
        {
            int kernelSize = 3;
            var filter = ENonLinearFilters.Sobel;
            string[] path = {
                Path.Combine(TestContext.CurrentContext.TestDirectory, JPG10x9),
                Path.Combine(TestContext.CurrentContext.TestDirectory, JPGJENKINS)
            };

            var imageResults = LMIPProcessor.NonLinearFilterBatch(filter, path, kernelSize);

            Assert.AreEqual(2, imageResults.Count());
            Assert.IsNotNull(imageResults[0].GetImage());
            Assert.IsNotNull(imageResults[1].GetImage());
        }

        [Test]
        public void NonLinearFilterBatchReturnsOneImage()
        {
            int kernelSize = 3;
            var filter = ENonLinearFilters.Sobel;
            string[] path = {
                Path.Combine(TestContext.CurrentContext.TestDirectory, JPG10x9)
            };

            var imageResults = LMIPProcessor.NonLinearFilterBatch(filter, path, kernelSize);

            Assert.AreEqual(1, imageResults.Count());
            Assert.IsNotNull(imageResults[0].GetImage());
        }

        [Test]
        public void NonLinearFilterBatchReturnsZeroImage()
        {
            int kernelSize = 3;
            var filter = ENonLinearFilters.Sobel;
            string[] path = { };

            var imageResults = LMIPProcessor.NonLinearFilterBatch(filter, path, kernelSize);

            Assert.AreEqual(0, imageResults.Count());
        }

        [Test]
        public void GifAnimationSavesGif()
        {
            var filter = ELinearFilters.InvertColors;
            string path = Path.Combine(TestContext.CurrentContext.TestDirectory, JPG10x9);

            LMIPGifAnimation.CreateAnimation(path, filter);

            Assert.IsTrue(File.Exists(path + "testi.gif"));
        }


        [Test, TestCaseSource(typeof(TestData), "TestCasesImageFormat")]
        public ImageFormat SobelFilterSavesImageInCorrectFormat(EImageFormat lmipFormat)
        {
            string imageSavePath = Path.Combine(TestContext.CurrentContext.TestDirectory, outputPath + "sobel." + lmipFormat.ToString());

            image.Save(imageSavePath, lmipFormat);

            Image img = Bitmap.FromFile(imageSavePath);
            return img.RawFormat;
        }

    }

    public class TestData
    {
        public static IEnumerable TestCasesImageFormat
        {
            get
            {
                yield return new TestCaseData(EImageFormat.Jpeg).Returns(ImageFormat.Jpeg);
                yield return new TestCaseData(EImageFormat.Png).Returns(ImageFormat.Png);
                yield return new TestCaseData(EImageFormat.Bmp).Returns(ImageFormat.Bmp);
                yield return new TestCaseData(EImageFormat.Tiff).Returns(ImageFormat.Tiff);
            }
        }
    }
}
