﻿using LMIP;
using LMIP.Interfaces;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMIPDemo
{
    class MyNonLmipOriginalSuperHeroUltraFilter : ILMIPLinearFilter
    {
        int m_KernelSize;
        LMIPImage m_Image;

        public int KernelSize { get { return m_KernelSize; } set { m_KernelSize = value; } }
        public LMIPImage Image { get { return m_Image; } set { m_Image = value; } }

        public void ApplyKernel(int x, int y, int width, int height)
        {
            for (int myX = x; myX < width; ++myX)
                for (int myY = y; myY < height; ++myY)
                {
                    Color oldColor = m_Image.GetPixel(x, y);
                    m_Image.SetPixel(x, y, Color.FromArgb(255 - oldColor.R, 255 - oldColor.G, 255 - oldColor.B));
                }
        }
        public void PostProcess()
        {
        }
        public void PreProcess()
        {
        }
      
    }

}
