﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LMIPDemo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public class ComboboxItem
        {
            public string Text { get; set; }
            public object Value { get; set; }

            public override string ToString()
            {
                return Text;
            }
        }

        private void addFilterToComboBox(string s, object o)
        {
            ComboboxItem filter = new ComboboxItem();
            filter.Text = s;
            filter.Value = o;

            comboBox1.Items.Add(filter);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;

            addFilterToComboBox("Invert Colors (linear)", LMIP.ELinearFilters.InvertColors);
            addFilterToComboBox("Optimize Contrast (linear)", LMIP.ELinearFilters.OptimizeContrast);
            addFilterToComboBox("Median (non-linear)", LMIP.ENonLinearFilters.Median);
            addFilterToComboBox("Sobel (non-linear)", LMIP.ENonLinearFilters.Sobel);

            comboBox1.SelectedIndex = 0;

            listView1.View = View.LargeIcon;
            imageList1.ImageSize = new Size(128, 128);
            listView1.LargeImageList = imageList1;

        }



        string sourcePicturePath="";

        private void button1_Click(object sender, EventArgs e)
        {
            FileDialog fileDialog = new OpenFileDialog();

            if(fileDialog.ShowDialog()==DialogResult.Cancel)
            {
                return;
            }

            sourcePicturePath = fileDialog.FileName;

            Image image = Bitmap.FromFile(sourcePicturePath);

            pictureBox1.Image = image;
            trackBar1.SetRange(1, Math.Min(image.Width, image.Height));
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FileDialog fileDialogSave = new SaveFileDialog();
            if (fileDialogSave.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }
            pictureBox2.Image.Save(fileDialogSave.FileName+".bmp", ImageFormat.Jpeg);
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            label2.Text = trackBar1.Value.ToString();
            pictureBox1.Invalidate();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if(File.Exists(sourcePicturePath) == false)
            {
                return;
            }

            LMIP.LMIPImage theImage=null;

            var value = ((ComboboxItem)comboBox1.SelectedItem).Value;

            if ( value.GetType() ==  typeof(LMIP.ELinearFilters) )
            {
                theImage = LMIP.LMIPProcessor.LinearFilter((LMIP.ELinearFilters)value, sourcePicturePath, trackBar1.Value);
            }
            else if (value.GetType() == typeof(LMIP.ENonLinearFilters))
            {
                theImage = LMIP.LMIPProcessor.NonLinearFilter((LMIP.ENonLinearFilters)value, sourcePicturePath, trackBar1.Value);
            }
   
            pictureBox2.Image = theImage.GetImage();

            //MyNonLmipOriginalSuperHeroUltraFilter snickers = new MyNonLmipOriginalSuperHeroUltraFilter();
            //LMIP.LMIPImage theImage = LMIP.LMIPProcessor.linearFilter(snickers, path,500);


        }

        private void pictureBox2_Paint(object sender, PaintEventArgs e)
        {
         
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawRectangle(Pens.Red, new Rectangle(0, 0, trackBar1.Value, trackBar1.Value));
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Multiselect = true;

            if(dialog.ShowDialog()==DialogResult.Cancel)
            {
                return;
            }
            string[] fileNames = dialog.FileNames;
            for (int i = 0; i < fileNames.Length; ++i)
            {
                listBox1.Items.Add(fileNames[i]);

            }
                    

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            if(listBox1.Items.Count==0)
            {
                return;
            }
            string[] fileNames = new string[listBox1.Items.Count];
            for (int i = 0; i < fileNames.Length; ++i)
            {
                fileNames[i] = listBox1.Items[i].ToString();
            }

            LMIP.LMIPImage[] results = LMIP.LMIPProcessor.LinearFilterBatch(LMIP.ELinearFilters.InvertColors, fileNames, 1);

            if (fileNames.Length != results.Length)
            {
                return;
            }


            listView1.Clear();
            imageList1.Images.Clear();
            for (int i = 0; i < results.Length; ++i)
            {
                string newFilename = fileNames[i] + "inv.jpeg";
                //    results[i].GetImage().Save(newFilename, ImageFormat.Jpeg);
                Image image = results[i].GetImage();
                imageList1.Images.Add(fileNames[i],image);

               var listViewItem = new ListViewItem("", fileNames[i]);
                   listView1.Items.Add(listViewItem);
                
            }
            
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (listBox1.Items.Count == 0)
            {
                return;
            }

            LMIP.Animation.LMIPGifAnimation.CreateAnimation(listBox1.Items[0].ToString(), LMIP.ELinearFilters.InvertColors);

           // if (fileNames.Length != results.Length)
           // {
           //     return;
           // }
           // for (int i = 0; i < results.Length; ++i)
           // {
           //     string newFilename = fileNames[i] + "inv.jpeg";
           //     results[i].GetImage().Save(newFilename, ImageFormat.Jpeg);
           // }
        }
    }
}
