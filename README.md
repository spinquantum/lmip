[![Build status](https://ci.appveyor.com/api/projects/status/74qfabq6qioprewl/branch/master?svg=true)](https://ci.appveyor.com/project/theseparator/lmip/branch/master)
[![Coverage Status](https://coveralls.io/repos/bitbucket/spinquantum/lmip/badge.svg?branch=master)](https://coveralls.io/bitbucket/spinquantum/lmip?branch=master)

# LMIP - **L**ightweight **M**ulti-threaded **I**mage **P**rocessing library


## Description

LMIP - is a .NET library for image processing (based on .NET Framework 4.5).  
  
### Features:  
* Easy to integrate in code, see description below 
* Extensible: add custom image filters 
* Lightweight: no additional libraries needed 
* Fast: optimal usage of threads 
* Process a single image or a collection of images 
* Supported image formats: JPEG, PNG, BMP, TIFF 
* pre-configured filters:  
    * linear: Contrast-Optimizing, Color-Inversion  
    * non linear: Median Filter, Edge-Detection (Sobel)  

---

## Usage

LMIP is easiest to use when installed via nuget. Please search for LMIP.  
For a detailed example on how this library can be used, see the LMIPDemo Application in this repository.  

#### Example for using a filter  
   
>     int kernelSize = 3;  
>     var filter = ENonLinearFilters.Median;  
>     string path = "image.jpg";  
>     var imageResult = LMIPProcessor.NonLinearFilter(filter, path, kernelSize);  
  
#### Adding a new filter
  
Depending on the filter you want to add, implement either the interface `ILMIPLinearFilter` or `ILMIPNonLinearFilter`.  
  

---

## Contribute to this project

To contribute to LMIP, clone this repo locally and commit your code on a separate branch and submit a pull request.  
Please write unit tests for your code!

---

## Licence: MIT

Copyright 2018 LMIP Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.